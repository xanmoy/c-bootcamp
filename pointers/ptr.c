#include <stdio.h>

void _wrong_swap(int a, int b)
{
    int temp;
    temp = a;
    a = b;
    b = temp;
    
}

int swap(int *a, int *b)
{
    int temp;
    temp = *a;
    *a = *b;
    *b = temp;
}
int main()
{
    int x = 4, y = 7;

    printf("The value of a and b before swapping is %d\n an %d\n", x, y);
    // _wrong_swap(x,y);
    swap(&x,&y);
    printf("The value of a and b after swapping is %d\n an %d\n", x, y);
    return 0;
}